/*jslint vars: true, plusplus: true, devel: true, nomen: true, regexp: true, indent: 4, maxerr: 50 */
/*global define, $, brackets, window */

define(function (require, exports, module) {
    'use strict';

    // Core Modules
    var LanguageManager = brackets.getModule('language/LanguageManager'),
        CodeMirror = brackets.getModule('thirdparty/CodeMirror2/lib/codemirror');

    var DustMode = require('dust');

    var ejsLanguage = LanguageManager.getLanguage('ejs');
    ejsLanguage.removeFileExtension('dust');

    LanguageManager.defineLanguage('dust_html', {
        name: 'Dust.js',
        mode: 'dustembedded',
        fileExtensions: ['dust']
    });

    LanguageManager.defineLanguage('dust', {
        name: 'Dust.js Parser',
        mode: 'dust',
        blockComment: ['{!', '!}']
    });


    // NOTE: This is an ugly hack to prevent the "internal" dust parsing mode from showing in the languages list.
    var originalGetLanguages = LanguageManager.getLanguages;
    LanguageManager.getLanguages = function () {
        var languages = originalGetLanguages();

        if (languages.dust) {
            delete languages.dust;
        }

        return languages;
    };
});
