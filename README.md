# Dust.js Syntax Highlighting for Brackets.io

This extension removes the (currently) limited support in Brackets.IO for Dust.js templates and replaces it with more comprehensive syntax highlighting.

Presently, this extension correctly identifies Dust.js tags and switches to an appropriate CodeMirror mode. In future releases, better support for in-tag highlighting will be included.

## Screenshots

![Screenshot of syntax highlighting in v0.1.0](http://i.imgur.com/396JNzd.png)
