/*global define, $, brackets, window, console */

define(function (require, exports, module) {
    'use strict';

    var CodeMirror = brackets.getModule('thirdparty/CodeMirror2/lib/codemirror'),
        multiplex = require('multiplex'),
        dustTagMap = {
            // Implied value "reference" for else
            '!': 'comment',
            '#': 'section',
            '/': 'close',
            ':': 'body',
            '?': 'exists',
            '^': 'not-exists',
            '@': 'helper',
            '+': 'inline-partial-definition',
            '<': 'inline-partial',
            '>': 'partial',
            '~': 'special'
        };

    CodeMirror.defineMode('dust', function (config, parserConfig) {
        return {
            startState: function () {
                return {
                    tagKind: null
                };
            },
            token: function (stream, state) {
                if (state.tagKind === null) {
                    state.tagKind = dustTagMap[stream.next()];

                    if (!state.tagKind) {
                        stream.backUp(1);
                        state.tagKind = 'reference';
                    } else if (state.tagKind === 'comment' || state.tagKind === 'special') {
                        stream.backUp(1);
                    } else {
                        return 'tag';
                    }
                }

                if (state.tagKind === 'reference') {
                    stream.next();
                    return 'property';
                } else if (state.tagKind === 'special') {
                    stream.next();
                    return 'atom';
                } else if (state.tagKind === 'comment') {
                    stream.next();
                    return 'comment';
                } else {
                    stream.next();
                    return null;
                }

                return null;
            }
        };
    });

    CodeMirror.defineMode('dustembedded', function (config, parserConfig) {
        return multiplex(
            CodeMirror.getMode(config, 'htmlmixed'),
            {
                open: /\{/,
                close: /\/?\}/,
                delimStyle: 'tag bracket',
                mode: CodeMirror.getMode(config, 'dust')
            }
        );
    });
});
