# 0.2.0
- Use local copy of CodeMirror multiplexing mode to allow future parsing of nested tags.
- Implement incredibly basic tag matching using new multiplexor.

# 0.1.1
- Refactor Regular Expressions into reusable, modular chunks.
- Enchance token matching to highlight start/end of tags.

# 0.1.0
- Initial support for block comments.
- Initial support for tags.
